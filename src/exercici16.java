import java.util.Scanner;

public class exercici16 {

	public static void main(String[] args) {

		Scanner scn = new Scanner(System.in);

		String frase;
		char caracter;
		int countLA = 0;
		int estat = 0;

		System.out.println("Digues una frase:");
		frase = scn.nextLine();

		for (int i = 0; i < frase.length(); i++) {

			// Agafa el caracter a la i posicio i converteix-la a minuscula
			caracter = Character.toLowerCase(frase.charAt(i));

			switch (estat) {
			case 0:
				if (caracter == 'l')
					estat = 1;
				break;
			case 1:
				if (caracter != 'l') {
					if (caracter == 'a')
						countLA++;
					estat = 0;
				}
				break;
			case 2:
				if (caracter == 'l')
					if (caracter == 's')
						countLA++;
					estat = 0;
				break;
			}
		}

		// Mostra quantes A's n'hi ha
		if (countLA == 0) {
			System.out.println("No n'hi ha cap LL");

		} else {
			System.out.println("N'hi ha " + countLA + " LL's");

		}

		scn.close();

	}

}