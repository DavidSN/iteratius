import java.util.Scanner;

public class exercici8 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int n, d;
		System.out.println("Digues un numero ");
		n = sc.nextInt();
		d = 1;
		for (int i = 0; i < n + 1; i++) {
			if (i == 0) {
				i = 1;
			}
			d = d * i;
		}

		System.out.println("Factorial " + d);

	}
}
